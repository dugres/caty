import sys
from io import StringIO
from contextlib import contextmanager

import caty


@contextmanager
def capture(fct, *args, **kwargs):
    bak_out, sys.stdout = sys.stdout, StringIO()
    bak_err, sys.stderr = sys.stderr, StringIO()
    try:
        fct(*args, **kwargs)
        yield sys.stdout.getvalue(), sys.stderr.getvalue()
    except Exception as x:
        print(type(x), x)
    finally:
        sys.stdout, sys.stderr = bak_out, bak_err

