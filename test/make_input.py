#!/usr/bin/python3
from random import randint
from os import remove, makedirs
import json, yaml, toml, pickle, sqlite3


def write(name, cont, mode='w'):
    with open(f'input/{name}', mode) as out:
        out.write(cont)


def dump(name, cont, dump, mode='w'):
    with open(f'input/{name}', mode) as out:
        dump(cont, out)


def txt():
    cont = '''
 A   B
123 abc
456 def
    '''.strip()
    write('check.txt', cont)


def csv():
    cont = '''
A,B
123,abc
456,def
    '''.strip()
    write('check.csv', cont)

def jsn():
    cont = [
        dict(
            A = 123,
            B = 'abc',
        ),
        dict(
            A = 456,
            B = 'def',
        ),
    ]
    dump('check.json', cont, json.dump)


def ini():
    cont = '''
[abc]
a = 123
b = 456

[def]
a = 741
b = 852
    '''.strip()
    write('check.ini', cont)


def yml():
    cont = [
        dict(
            A = 123,
            B = 'abc',
        ),
        dict(
            A = 456,
            B = 'def',
        ),
    ]
    dump('check.yaml', cont, yaml.dump)


def tml():
    cont = {"a": {"b": 1, "c": 2}}
    dump('check.toml', cont, toml.dump)


def bin():
    cont = bytes(randint(0,255) for _ in range(256))
    for ext in 'bin oct dec hex'.split():
        write(f'check.{ext}', cont, 'wb')


def pkl():
    cont = {"a": {"b": 1, "c": 2}}
    dump('check.pickle', cont, pickle.dump, 'wb')


def sql():
    db = 'input/check.sqlite'
    try:
        remove(db)
    except: pass
    cnx = sqlite3.connect(db)
    cur = cnx.cursor()
    exe = cur.execute
    exe('''
        create table dict (key text, val int)
    ''')
    exe('''
        insert into dict values('abc', 123)
    ''')
    cnx.commit()
    cnx.close()


def htm():
    cont = '''
    <html><body><div>
    <h1>a b</h1>
    <p>123 456</p>
    </div></body> </html>
    '''.strip()
    write('check.html', cont)


def make():
    makedirs('input', exist_ok=True)
    txt()
    csv()
    jsn()
    ini()
    yml()
    tml()
    bin()
    pkl()
    sql()
    htm()


if __name__=='__main__': make()
