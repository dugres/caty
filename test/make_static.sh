#!/bin/bash

if [ -d output ]; then
    rm output/*
else
    mkdir output
fi

for f in $(ls input); do
    echo $f
    caty input/$f > output/$f
done
